//
//  AuthenticationViewController.swift
//  TouchID
//
//  Created by Luigi Aiello on 15/09/17.
//  Copyright © 2017 Luigi Aiello. All rights reserved.
//

import UIKit
import LocalAuthentication

class AuthenticationViewController: UIViewController {

    //Mark:- Outlets
    @IBOutlet weak var touchIDButton: UIButton!
    @IBOutlet weak var label: UILabel!
    
    //Mark:- Variables
    var context = LAContext()
    deinit {
        Utils.removeObserverForNotifications(observer: self)
    }
    //Mark:- Keys
    let kMsgAuth = "Show me your finger"
    let kMsgShowReason = "Try to dismiss this screen"
    let kMsgFingerOK = "Login successful!"
    
    //Mark:- Override
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Mark:- Setup
    private func setup() {
        Utils.registerNotificationWillEnterForeground(observer: self, selector: #selector(AuthenticationViewController.updateUI))
    }
    @objc private func updateUI() {
        //> iOS9
        let policy: LAPolicy = .deviceOwnerAuthentication
        var err: NSError?
        
        // Check if the user is able to use the policy we've selected previously
        guard context.canEvaluatePolicy(policy, error: &err) else {
            touchIDButton.imageView?.image = #imageLiteral(resourceName: "TouchID_off")
            // Print the localized message received by the system
            label.text = err?.localizedDescription
            return
        }
        
        //The user is able to use his/her Touch ID
        touchIDButton.imageView?.image = #imageLiteral(resourceName: "TouchID_on")
        label.text = kMsgAuth
        
        loginProcess(policy: policy)
    }
    private func loginProcess(policy: LAPolicy) {
        //Start evaluation process with a callback that is executed when the user ends the process successfully or not
        context.evaluatePolicy(policy, localizedReason: kMsgShowReason, reply: { (success, error) in
            DispatchQueue.main.async {
                
                guard success else {
                    guard let error = error else {
                        self.showUnexpectedErrorMessage()
                        return
                    }
                    switch(error) {
                    case LAError.authenticationFailed:
                        self.label.text = "There was a problem verifying your identity."
                    case LAError.userCancel:
                        self.label.text = "Authentication was canceled by user."
                        // Fallback button was pressed and an extra login step should be implemented for iOS 8 users.
                    // By the other hand, iOS 9+ users will use the pasccode verification implemented by the own system.
                    case LAError.userFallback:
                        self.label.text = "The user tapped the fallback button (Fuu!)"
                    case LAError.systemCancel:
                        self.label.text = "Authentication was canceled by system."
                    case LAError.passcodeNotSet:
                        self.label.text = "Passcode is not set on the device."
                    case LAError.touchIDNotAvailable:
                        self.label.text = "Touch ID is not available on the device."
                    case LAError.touchIDNotEnrolled:
                        self.label.text = "Touch ID has no enrolled fingers."
                    // iOS 9+ functions
                    case LAError.touchIDLockout:
                        self.label.text = "There were too many failed Touch ID attempts and Touch ID is now locked."
                    case LAError.appCancel:
                        self.label.text = "Authentication was canceled by application."
                    case LAError.invalidContext:
                        self.label.text = "LAContext passed to this call has been previously invalidated."
                    // MARK: IMPORTANT: There are more error states, take a look into the LAError struct
                    default:
                        self.label.text = "Touch ID may not be configured"
                        break
                    }
                    return
                }
                
                //Auth success
                guard let destination = "Main" <%> "safe vc" as? SafeViewController else {
                    return
                }
                self.navigationController?.pushViewController(destination, animated: true)
            }
        })
    }
    private func showUnexpectedErrorMessage() {
        touchIDButton.imageView?.image = #imageLiteral(resourceName: "TouchID_off")
        label.text = "Unexpected error! 😱"
    }
    
    //Mark:- Actions
    @IBAction func touchIdDidTap(_ sender: Any) {
        context = LAContext()
        updateUI()
    }
}
